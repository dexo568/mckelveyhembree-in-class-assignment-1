<!DOCTYPE html>
<head>
<title>Battlefield Analysis</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body>
	<div id="main">
		<h1>
			Battlefield Analysis
		</h1>
		<h2>
			Latest Critiques
		</h2>
		<?php
$mysqli = new mysqli('ec2-54-227-50-129.compute-1.amazonaws.com', 'TA', 'cse330rocks', 'battlefield');
 
			if($mysqli->connect_errno) {
				printf("Connection Failed: %s\n", $mysqli->connect_error);
				exit;
			}
			$stmt = $mysqli->prepare("select critique from reports order by id desc limit 5");
			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$stmt->execute();
 
			$stmt->bind_result($critiques);
			echo "<ul>\n";
			while($stmt->fetch()){
				printf("\t<li>%s</li>\n",
				htmlspecialchars($critiques)
				);
			}
			echo "</ul>\n";
			$stmt->close();
		?>
		<h2>
			Battle Statistics
		</h2>
		<a href="battlefield-submit.html">Submit a New Battle Report</a>
	</div>
</body>
</html>