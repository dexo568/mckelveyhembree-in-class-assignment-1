<?php
if(!(isset($_POST['ammo'])&&isset($_POST['soldiers'])&&isset($_POST['duration'])&&isset($_POST['critique']))){
echo "Post variables not present!";
exit;
}
$ammo= (int) $_POST['ammo'];
$soldiers= (int) $_POST['soldiers'];
$duration= (float) $_POST['duration'];
$critique=$_POST['critique'];

$mysqli = new mysqli('ec2-54-227-50-129.compute-1.amazonaws.com', 'TA', 'cse330rocks', 'battlefield');
 
if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}
$stmt= $mysqli->prepare("insert into reports (ammunition, soldiers, duration, critique) values (?,?,?,?)");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->bind_param('iids', $ammo, $soldiers, $duration, $critique);
 
$stmt->execute();
if($stmt){
	echo "Success";
	header( 'Location:battlefield-submit.html' );
}
else{
echo "Error";
exit;
}
$stmt->close();

?>